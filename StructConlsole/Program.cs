﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructConsole
{
    class Program
    {
        struct Date
        {
            public int Year;
            public int Month;
            public int Day;
            public int Hours;
            public int Minutes;
            public Date(int year, int month, int day, int hours, int minutes)
            {
                Year = year;
                Month = month;
                Day = day;
                Hours = hours;
                Minutes = minutes;
            }
        }
        struct Airplane
        {
            public string StartCity;
            public string FinishCity;
            public Date StartDate;
            public Date FinishDate;
            public Airplane(string startCity, string finishCity, Date startDate, Date finishDate)
            {
                StartCity = startCity;
                FinishCity = finishCity;
                StartDate = startDate;
                FinishDate = finishDate;
            }

            public double GetTotalTime()
            {
                DateTime departure = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartDate.Hours, StartDate.Minutes, 0);
                DateTime arrival = new DateTime(FinishDate.Year, FinishDate.Month, FinishDate.Day, FinishDate.Hours, FinishDate.Minutes, 0);
                TimeSpan travelTime = arrival - departure;
                double totalTime = travelTime.TotalMinutes;
                return totalTime;
            }
            public bool IsArrivingToday()
            {
                if (StartDate.Day == FinishDate.Day && StartDate.Month == FinishDate.Month && StartDate.Year == FinishDate.Year)
                    return true;
                else
                    return false;

            }
        }
        static Airplane[] ReadAirplaneArray(params Airplane[] arr)
        {
            bool isCorrect;
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"ЗАПИС №{i+1}");
                Console.Write("Місто відправлення: ");
                arr[i].StartCity = Console.ReadLine();
                Console.WriteLine("Дата відправлення");
                do
                {
                    Console.Write("Рік: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].StartDate.Year);
                    if (!isCorrect || arr[i].StartDate.Year < 2017)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Year < 2017);
                do
                {
                    Console.Write("Місяць: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].StartDate.Month);
                    if (!isCorrect || arr[i].StartDate.Month < 1 || arr[i].StartDate.Month > 12)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Month < 1 || arr[i].StartDate.Month > 12);
                do
                {
                    Console.Write("День: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].StartDate.Day);
                    if (!isCorrect ||arr[i].StartDate.Day < 1 || arr[i].StartDate.Day > 31)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Day < 1 || arr[i].StartDate.Day > 31);
                do
                {
                    Console.Write("Години: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].StartDate.Hours);
                    if (!isCorrect || arr[i].StartDate.Hours < 1 || arr[i].StartDate.Hours > 24)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Hours < 1 || arr[i].StartDate.Hours > 24);
                do
                {
                    Console.Write("Хвилини: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].StartDate.Minutes);
                    if (!isCorrect || arr[i].StartDate.Minutes < 0 || arr[i].StartDate.Minutes > 59)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Minutes < 0 || arr[i].StartDate.Minutes > 59);
                Console.WriteLine("----------------------------------------------------");
                Console.Write("Місто прибуття: ");
                arr[i].FinishCity = Console.ReadLine();
                Console.WriteLine("Дата прибуття");
                do
                {
                    Console.Write("Рік: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].FinishDate.Year);
                    if (!isCorrect || arr[i].FinishDate.Year < arr[i].StartDate.Year)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].FinishDate.Year < arr[i].StartDate.Year);
                do
                {
                    Console.Write("Місяць: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].FinishDate.Month);
                    if (!isCorrect || arr[i].FinishDate.Month < 1 || arr[i].FinishDate.Month > 12 || arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Month < arr[i].StartDate.Month)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].FinishDate.Month < 1 || arr[i].FinishDate.Month > 12 || arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Month < arr[i].StartDate.Month);
                do
                {
                    Console.Write("День: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].FinishDate.Day);
                    if (!isCorrect || arr[i].FinishDate.Day < 1 || arr[i].FinishDate.Day > 31 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day < arr[i].StartDate.Day)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].FinishDate.Day < 1 || arr[i].FinishDate.Day > 31 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day < arr[i].StartDate.Day);
                do
                {
                    Console.Write("Години: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].FinishDate.Hours); 
                    if (!isCorrect || arr[i].FinishDate.Hours < 1 || arr[i].FinishDate.Hours > 24 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day == arr[i].StartDate.Day && arr[i].FinishDate.Hours < arr[i].StartDate.Hours)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].StartDate.Hours < 1 || arr[i].StartDate.Hours > 24 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day == arr[i].StartDate.Day && arr[i].FinishDate.Hours < arr[i].StartDate.Hours);
                do
                {
                    Console.Write("Хвилини: ");
                    isCorrect = int.TryParse(Console.ReadLine(), out arr[i].FinishDate.Minutes);
                    if (!isCorrect || arr[i].FinishDate.Minutes < 0 || arr[i].FinishDate.Minutes > 59 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day == arr[i].StartDate.Day && arr[i].FinishDate.Hours == arr[i].StartDate.Hours && arr[i].FinishDate.Minutes < arr[i].StartDate.Minutes)
                        Console.WriteLine("Помилка! Введіть дані коректно.");
                } while (!isCorrect || arr[i].FinishDate.Minutes < 0 || arr[i].FinishDate.Minutes > 59 || arr[i].FinishDate.Month == arr[i].StartDate.Month && arr[i].FinishDate.Year == arr[i].StartDate.Year && arr[i].FinishDate.Day == arr[i].StartDate.Day && arr[i].FinishDate.Hours == arr[i].StartDate.Hours && arr[i].FinishDate.Minutes < arr[i].StartDate.Minutes);
                Console.WriteLine("----------------------------------------------------");
            }
            return arr;
        }
        static void PrintAirplane(Airplane airplane)
        {
            Console.WriteLine($"Місто відправлення: {airplane.StartCity}");
            Console.WriteLine($"Дата відправлення: {airplane.StartDate.Day}.{airplane.StartDate.Month}.{airplane.StartDate.Year}");
            Console.WriteLine($"Час відправлення: {airplane.StartDate.Hours}:{airplane.StartDate.Minutes}");
            Console.WriteLine($"Місто прибуття: {airplane.FinishCity}");
            Console.WriteLine($"Дата прибуття: {airplane.FinishDate.Day}.{airplane.FinishDate.Month}.{airplane.FinishDate.Year}");
            Console.WriteLine($"Час прибуття: {airplane.FinishDate.Hours}:{airplane.FinishDate.Minutes}");            
        }
        static void PrintAirplanes(Airplane[] arr)
        {
            foreach(Airplane airplane in arr)
            {
                PrintAirplane(airplane);
            }
        }
        static void GetAirplaneInfo(Airplane[] arr, out double minTravelTime, out double maxTravelTime)
        {
            minTravelTime = arr[0].GetTotalTime();
            maxTravelTime = arr[0].GetTotalTime();
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i].GetTotalTime() < minTravelTime)
                    minTravelTime = arr[i].GetTotalTime();
                if (arr[i].GetTotalTime() > maxTravelTime)
                    maxTravelTime = arr[i].GetTotalTime();
            }
        }
        static int ComparatorSortAirplanesByDateOfDeparture(Airplane a, Airplane b)
        {
            if ((a.StartDate.Year > b.StartDate.Year) || (a.StartDate.Year == b.StartDate.Year && a.StartDate.Month > b.StartDate.Month) || (a.StartDate.Year == b.StartDate.Year && a.StartDate.Month == b.StartDate.Month && a.StartDate.Day > b.StartDate.Day))
                return 1;
            if ((a.StartDate.Year < b.StartDate.Year) || (a.StartDate.Year == b.StartDate.Year && a.StartDate.Month < b.StartDate.Month) || (a.StartDate.Year == b.StartDate.Year && a.StartDate.Month == b.StartDate.Month && a.StartDate.Day < b.StartDate.Day))
                return -1;
            return 0;
        }
        static int ComparatorSortAirplanesByTotalTime(Airplane a, Airplane b)
        {
            double totalTimeA = a.GetTotalTime(), totalTimeB = b.GetTotalTime();
            if (totalTimeA > totalTimeB)
                return 1;
            if (totalTimeA < totalTimeB)
                return -1;
            return 0;
        }
        static void SortAirplanesByDate(Airplane[] arr)
        {
            Array.Sort(arr, ComparatorSortAirplanesByDateOfDeparture);
            Array.Reverse(arr);
        }
        static void SortAirplanesByTotalTime(Airplane[] arr)
        {
            Array.Sort(arr, ComparatorSortAirplanesByTotalTime);
        }

        enum Operation
        {
            Read = 1,
            PrintOne,
            PrintArray,
            Time,
            DepartureArrival,
            MinMax,
            SortDate,
            SortTime,
            Exit
        }

        static void Main(string[] args)
        {
            Console.Title = "Лабораторна робота №4";
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;            
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            uint n, number;
            bool isTrue;
            Operation op;
            do
            {
                Console.Write("Введіть кількість записів:\n--> ");
                isTrue = uint.TryParse(Console.ReadLine(), out n);
                if (!isTrue || n < 1)
                    Console.WriteLine("Помилка! Повторіть введення значення.");
            } while (!isTrue || n < 1);
            Airplane[] array = new Airplane[n];
            do
            {
                Console.WriteLine("Оберіть дію:\n1 - Заповнити масив структур\n2 - Вивести одну структуру на екран\n3 - Вивести масив структур на екран\n4 - Обрахувати сумарний час подорожі у хвилинах\n5 - Дізнатись, чи прибуття й відправлення в один і той же день\n6 - Знайти найменший та найбільший час подорожі\n7 - Відсортувати масив структур за спаданням дати відправлення\n8 - Відсортувати масив структур за зростанням часу подорожі\n9 - Вийти з програми");
                do
                {
                    Console.Write("--> ");
                    isTrue = Operation.TryParse(Console.ReadLine(), out op);
                    if (!isTrue)
                        Console.WriteLine("Такої дії немає! Введіть значення ще раз.");
                } while (!isTrue);

                switch (op)
                {
                    case Operation.Read:
                        ReadAirplaneArray(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.PrintOne:
                        do
                        {
                            Console.WriteLine($"Введіть номер структури (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такої структури немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        PrintAirplane(array[number]);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.PrintArray:
                        PrintAirplanes(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.Time:
                        do
                        {
                            Console.WriteLine($"Введіть номер структури (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такої структури немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        Console.WriteLine($"Час подорожі структури №{number+1}: {array[number].GetTotalTime()} хв.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.DepartureArrival:
                        do
                        {
                            Console.WriteLine($"Введіть номер структури (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такої структури немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        if (array[number].IsArrivingToday())
                            Console.WriteLine("Прибуття й відправлення в один і той же день");
                        else
                            Console.WriteLine("Прибуття й відправлення в різні дні");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.MinMax:
                        double minTime, maxTime;
                        do
                        {
                            Console.WriteLine($"Введіть номер структури (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такої структури немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        GetAirplaneInfo(array, out minTime, out maxTime);
                        Console.WriteLine($"Найменший час подорожі: {minTime} хв.\nНайбільший час подорожі: {maxTime} хв.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.SortDate:
                        SortAirplanesByDate(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.SortTime:
                        SortAirplanesByTotalTime(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.Exit:
                        return;                       
                }
            } while (true);
        }
    }
}